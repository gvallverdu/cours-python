# Slides

Ce dossier contient les slides et les dépendances pour les faire tourner. Les
slides utilises `reveal.js`.

## Contenu des dossiers :

* **css** : css perso + css highlight du code source https://highlightjs.org/static/demo/
* **Font-Awesome** http://fontawesome.io/ : icones
* **fonts** les polices utilisées pour le theme
* **MathJax** => math avec latex dans html
* **reveal.js** => le framework pour les slides
* **html** => les slides

## submodules git

Les dossier MathJax, Font-Awesome et reveal.js sont des sous-modules git, voir
ici https://git-scm.com/book/fr/v2/Utilitaires-Git-Sous-modules .

Pour créer un nouveau sous-modules :

    git submodule add https://github.com/mathjax/MathJax.git
    git commit -am 'added new submodule mathjax'

Après avoir cloner cours-python, les dossier sous-modules sont vides. Pour cloner
avec les sous-modules

    git clone --recursive https://gitlab.com/gvallverdu/cours-python.git

Ou alors dans chaque dossir de sous-modules

    git submodule init
    git submodule updage
