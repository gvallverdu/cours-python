# Cours python

* Germain Vallverdu - germain.vallverdu@univ-pau.fr

Le cours et le matériel disponible ont pour objectif d'initier à la programmation
avec le langage python. Un accent particulier est donné sur l'utilisation de ce 
langage dans le cadre du traitement des données et de leur représentation graphique.

Le cours est destiné à un public non spécialiste de la programmation. Ainsi, les
aspects techniques ou la façon dont sont implémentés les différents aspect du 
langage ne sont pas abordés. La priorité est donnée à la pratique du langage.

Pour aller plus loin, le cours *Python 3 : des fondamentaux aux concepts avancés du langage* 
accessible sur la plateforme [FUN, France Université Numérique](https://www.fun-mooc.fr)  
est vivement recommandé.

Le projet est organisé comme suit :

* notebooks : les jupyter notebooks pour le cours et une version html
* slides : slides pour illustrer divers points. Les slides sont dans
  slides/html/ et écrites en utilisant reveal.js
* cheat_sheet : un ensemble de fiches sur des sujets proches du cours.

## Quelle version de python

Ce cours a été construit en utilisant Python 3. C'est aujourd'hui la version
**recommandée** de python bien que de nombreuses librairies soient toujours 
disponibles en Python 2. 

## GitBook 

Une partie (moins à jour) de ce cours est disponible sous la forme d'un
livre numérique en ligne :

https://www.gitbook.com/book/gvallverdu/python_sciences/details

## Autres notebooks :

Voici une série de notebooks jupyter qui peuvent compléter le cours ou apporter
d'autres exemple :

http://nbviewer.jupyter.org/github/gvallverdu/cookbook/tree/master/

## Licence

Tout le materiel est mis à disposition selon les termes de la 
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">licence 
Creative Commons Attribution -  Partage dans les Mêmes 
Conditions 4.0 International</a>.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>