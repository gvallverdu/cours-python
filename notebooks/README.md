# Notebooks

Les notebooks sont en deux versions :

* les versions XX-notebook.ipynb sont à destination des étudiants
* les versions notebook.ipynb sont les corrections

## Progression

La numérotation donne une progression possible. La partie sur les objets et
classes peut être mise de côté en fonction du public. On pourrait même pousser
le vice jusqu'à commencer par Pandas, les instructions de bases de pandas ne
nécessitant pas nécessairement de connaître python.

* 00-Calculatrice.ipynb : découverte de jupyter notebook et du prompt python
* 01-Morse.ipynb : notion de variables et conditions avec une première fonction
* 02-arbre-noel.ipynb : notion de boucles et fonctions
* 03-objets-classes.ipynb : découverte des classes python
* 04-dictionnaires.ipynb : utilisation d'un dictionnaire
* pd01-pandas-intro.ipynb : découverte de pandas
* pd02-pandas-paris.ipynb : traitements de données sur paris



