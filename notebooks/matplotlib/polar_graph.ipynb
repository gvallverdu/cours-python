{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#96CDF2; padding:10px;color:#3B3C3E\">\n",
    "Licence <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA</a>\n",
    "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"Licence Creative Commons\" style=\"border-width:0; float:right;\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /></a><br />\n",
    "Germain Salvato Vallverdu - <tt>germain.vallverdu@univ-pau.fr</tt> <br />\n",
    "<i><a href=\"http://iprem.univ-pau.fr/fr/index.html\">IPREM</a> / <a href=\"http://www.univ-pau.fr/\">Univ Pau &amp; Pays Adour</a></i>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Un graphique en coordonnées polaires\n",
    "\n",
    "Ce Notebook présente la construction d'un graphique en coordonnées polaires avec matplotlib."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Coordonnées polaires, un rappel\n",
    "\n",
    "--- \n",
    "\n",
    "Le système de coordonnées polaires, est un système de coordonnées à deux dimensions dans lequel la position d'un point est déterminée par rapport à un angle, $\\theta$, et une distance, $r$.\n",
    "\n",
    "Vocabulaires ou définitions :\n",
    "\n",
    "* Pôle : point central, correspond à l'origine des coordonnées cartésiennes\n",
    "* coordonnée radiale : notée $r$ ou $\\rho$ et appelée rayon, est la distance entre le point et le pôle\n",
    "* coordonnée angulare ou angle polaire ou azimut : notée $\\theta$ ou $t$, c'est l'angle orienté, dans le sens trigonométrique, anti-horaire, entre la demi-droite passant par le point et le pôle et la demi-droite d'angle $\\theta=0^o$, appelée axe polaire (équivalent de l'axe des abscisses en coordonnées cartésiennes.\n",
    "\n",
    "Pour rappel, on passe des coordonnées cartésiennes aux coordonnées polaires par :\n",
    "\n",
    "\\begin{cases}\n",
    "    x = r \\cos\\theta \\\\\n",
    "    y = r \\sin\\theta\n",
    "\\end{cases}\n",
    "\n",
    "<div class=\"alert alert-success\">\n",
    "<b>Exercice :</b> placer les points suivants en utilisant les coordonnées polaires :\n",
    "\n",
    "$\\left(0; 1 \\right)$\n",
    "$\\left(\\frac{5\\pi}{6}; \\frac{1}{2} \\right)$\n",
    "$\\left(\\frac{\\pi}{4}; \\frac{3}{4} \\right)$\n",
    "$\\left(\\frac{3\\pi}{2}; \\frac{1}{5} \\right)$\n",
    "$\\left(\\frac{7\\pi}{5}; \\frac{3}{4} \\right)$\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Coordonnées polaires avec matplotlib\n",
    "\n",
    "---\n",
    "\n",
    "Matplotlib dispose des outils nécessaires pour faire un graphique en coordonnées polaires. La façon la plus simple est d'utiliser la fonction `plt.polar()` du module `matplotlib.pyplot`. Les valeurs de $\\theta$ doivent être données en radian. Il existe dans le module `numpy` des fonctions qui font la conversion entre radians et degrés ainsi que la constante $\\pi$ :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print(\"pi = \", np.pi)\n",
    "print(\"60 degres = \", np.radians(60), \" radians\")\n",
    "print(\"pi / 3 radians = \", np.degrees(np.pi / 3), \" degres\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 Des points\n",
    "\n",
    "Par exemple, voici un point en $\\left(\\frac{\\pi}{3}, 1\\right)$ :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "plt.polar(np.pi / 3, 1, marker=\"D\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "<b>Exercice :</b> Reprendre l'exercice précédent et placer les points suivants avec matplotlib :\n",
    "\n",
    "$\\left(0; 1 \\right)$\n",
    "$\\left(\\frac{5\\pi}{6}; \\frac{1}{2} \\right)$\n",
    "$\\left(\\frac{\\pi}{4}; \\frac{3}{4} \\right)$\n",
    "$\\left(\\frac{3\\pi}{2}; \\frac{1}{5} \\right)$\n",
    "$\\left(\\frac{7\\pi}{5}; \\frac{3}{4} \\right)$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Une fonction polaire\n",
    "\n",
    "De la même manière que pour un graphique en coordonnées cartésiennes, il est possible de tracer une fonction. Néanmoins, les valeurs de $r(\\theta)$ doivent être strictement positives.\n",
    "\n",
    "Par exemple voici la fonction \n",
    "\n",
    "$$r(\\theta) = \\left\\vert 3 \\cos^2\\theta - 1 \\right\\vert$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(6, 6))\n",
    "theta = np.linspace(0, 2 * np.pi, 200)\n",
    "r =  np.abs(3 * np.cos(theta)**2 - 1)\n",
    "plt.polar(theta, r)\n",
    "plt.suptitle(r\"$r(\\theta) = \\left\\vert 3 \\cos^2\\theta - 1 \\right\\vert$\", fontsize=20)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 La version orientée objet\n",
    "\n",
    "Pour tracer un graphique en coordonnées polaires, il est préférable d'utiliser la syntaxe orientée objet de matplotlib. Cette syntaxe offre des options de configuration avancées pour contrôler les axes et la grille d'un graphique en coordonnées polaires.\n",
    "\n",
    "Voici comment s'écrit l'exemple ci-dessus, en utilisant cette syntaxe. Deux éléments à bien noter :\n",
    "\n",
    "* On utilise ici `ax.plot` et non `ax.polar`.\n",
    "* Pour les fonctions permettant de contrôler la grille, les valeurs de $\\theta$ sont en degrés."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(6, 6))\n",
    "fig.suptitle(r\"$r(\\theta) = \\left\\vert 3 \\cos^2\\theta - 1 \\right\\vert$\", fontsize=20)\n",
    "\n",
    "theta = np.linspace(0, 2 * np.pi, 200)\n",
    "r = 3 * np.cos(theta)**2 - 1\n",
    "\n",
    "ax = fig.add_subplot(111, projection=\"polar\")\n",
    "ax.plot(theta, np.abs(r))\n",
    "\n",
    "# controles de la grilles:\n",
    "ax.set_rlim((0, 2.5))\n",
    "ax.set_rgrids([.5, 1, 1.5, 2.], angle=270)\n",
    "ax.set_thetagrids([0, 54.74, 90, 180 - 54.74, 180, 180 + 54.74, 270, 360 - 54.74])\n",
    "ax.set_theta_zero_location(\"N\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Quelques courbes\n",
    "\n",
    "---\n",
    "\n",
    "Les courbes usuelles les plus simples en coordonnées polaires sont la rosace et la spirale d'Archimède.\n",
    "\n",
    "La rosace a pour équation :\n",
    "\n",
    "$$r(\\theta) = a \\cos\\left(k\\theta + \\phi_o\\right)$$\n",
    "\n",
    "où $k$ est un entier qui détermine le nombre de pétales, $a$ la taille et $\\phi_o$ fait tourner l'ensemble.\n",
    "\n",
    "La spirale d'Archimède a pour équation :\n",
    "\n",
    "$$r(\\theta) = a + b\\theta$$\n",
    "\n",
    "où $a$ détermine le point de départ sur la droite $\\theta=0$ et $b$ l'écart entre deux bras successifs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "<b>Exercice :</b> Tracer une rosace.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "<b>Complément :</b> La rosace devrait avoir k branches si k est impair et 2k branches si k est pair. \n",
    "Pour le vérifier, il faut utiliser la convention suivante : si $r < 0$ alors $\\theta = \\theta + \\pi$. Consulter la documentation de la fonction `np.where()` pour voir comment appliquer efficacement cette condition.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "<b>Exercice :</b> Tracer une spirale d'archimede et changer les valeurs de a et b pour voir l'effet sur la courbe.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "**Exercice : ** Tracer le papillon de T. Fay pour $\\theta\\in[0; 18\\pi]$ dont l'équation est la suivante :\n",
    "\n",
    "$$r(\\theta) = e^{\\cos\\theta} - 2\\cos4\\theta + \\sin^5\\frac{\\theta}{12}$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "**Exercice : ** Tracer des trèfles de Habenicht, dont le nombre de pétales est déterminé par $n$ dans l'équation suivante :\n",
    "\n",
    "$$r(\\theta) = 1 + \\cos n \\theta + \\sin^2 n \\theta$$\n",
    "\n",
    "Exercez vous aux points suivants :\n",
    "<ul>\n",
    "<li>Écrire une fonction qui retourne $r(\\theta)$ et qui permet de choisir la valeur de $n$</li>\n",
    "<li>Faire plusieurs `subplots` pour différentes valeurs de $n$.</li>\n",
    "<li>Vous pouvez faire une boucle !</li>\n",
    "</ul>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
