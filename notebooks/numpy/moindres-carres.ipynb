{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#96CDF2; padding:10px;color:#3B3C3E\">\n",
    "Licence <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA</a>\n",
    "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"Licence Creative Commons\" style=\"border-width:0; float:right;\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /></a><br />\n",
    "Germain Salvato Vallverdu - <tt>germain.vallverdu@univ-pau.fr</tt> <br />\n",
    "<i><a href=\"http://iprem.univ-pau.fr/fr/index.html\">IPREM</a> / <a href=\"http://www.univ-pau.fr/\">Univ Pau &amp; Pays Adour</a></i>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comment mettre en oeuvre une régression linéaire avec python ?\n",
    "\n",
    "L'objectif de ce TP est de mettre en pratique le langage python pour réaliser une regression linéaire. L'idée est, dans un premier temps, de reprendre les éléments de base du langage (condition, boucles ...) pour créer un outil qui calcule les paramètres par la méthode des moindres carrés. Dans une deuxième partie les modules spécifiques tels que [numpy](http://www.numpy.org/) ou [matplotlib](http://matplotlib.org/) seront mis à profit pour réaliser la même opération avec des méthodes existantes.\n",
    "\n",
    "*Note : Ce notebook est compatible python2 et python3. Il est recommandé d'utiliser python3.*\n",
    "\n",
    "### Sommaire\n",
    "\n",
    " 1. [Introduction](#Introduction)\n",
    "     1. [Cahier des charges](#Cahier-des-charges)\n",
    "     2. [Rappels mathématiques](#Rappels-mathématiques)\n",
    "     3. [Progression](#Progression)\n",
    " 2. [Programation](#Programmation)\n",
    "     1. [Étape 0: Lecture du fichier de données](#Étape-0:-Lecture-du-fichier-donnees.dat)\n",
    "     2. [Étape 1: À la main](#Étape-1:-À-la-main)\n",
    "     3. [Étape 2: Créer une fonction](#Étape-2:-Créer-une-fonction)\n",
    "     4. [Étape 3: Utilisation du module numpy](#Étape-3:-Utilisation-du-module-numpy)\n",
    "     5. [Étape 4: Utilisation des méthodes prédéfinies dans numpy et scipy](#Étape-4:-Utilisation-des-méthodes-prédéfinies-dans-numpy-et-scipy)\n",
    " 3. [Représentation graphique](#Représentation-graphique)\n",
    " 4. [Pour aller plus loin ; les résidus](#Pour-aller-plus-loin-:-les-résidus)\n",
    " 5. [Conlusion](#Conclusion)\n",
    "\n",
    "## Introduction\n",
    "\n",
    "### Cahier des charges\n",
    "\n",
    "Le programme que nous allons écrire devra réaliser les opérations suivantes :\n",
    "\n",
    "* Lire les valeurs des valeurs de $x$ et $y$ sur le fichier `donnees.dat`.\n",
    "* Calculer les paramètres $a$ et $b$ de la régression linéaire par la méthode des moindres carrés et les afficher.\n",
    "* (bonus) Représenter les points $(x,y)$ et tracer la droite de régression.\n",
    "\n",
    "### Rappels mathématiques\n",
    "\n",
    "La régression linéaire consiste à chercher les paramètres $a$ et $b$ définissant la droite $y=ax+b$ qui passe au plus près d'un ensemble de points $(x_k,y_k)$. Les paramètres $a$ et $b$ sont déterminés par la méthodes des moindres carrés qui consiste, dans le cas d'une régression linéaire, à minimiser la quantité :\n",
    "\n",
    "\\begin{equation}\n",
    "    Q(a, b) = \\sum_{k=1}^N (y_k - a x_k - b)^2\n",
    "\\end{equation}\n",
    "\n",
    "Le minimum de $Q(a,b)$ est obtenu lorsque ses dérivées par rapport à $a$ et $b$ sont nulles. Il faut donc résoudre le système à deux équations deux inconnues suivant :\n",
    "\n",
    "\\begin{align}\n",
    "    &\n",
    "    \\begin{cases}\n",
    "        \\displaystyle\\frac{\\partial Q(a,b)}{\\partial a} = 0 \\\\ \n",
    "        \\displaystyle\\frac{\\partial Q(a,b)}{\\partial b} = 0\n",
    "    \\end{cases}\n",
    "    &\n",
    "    \\Leftrightarrow &\n",
    "    &\n",
    "    &\n",
    "    \\begin{cases}\n",
    "        \\displaystyle -2 \\sum_{k=1}^N x_k \\left(y_k - a x_k - b\\right) = 0 \\\\ \n",
    "        \\displaystyle -2 \\sum_{k=1}^N \\left(y_k - a x_k - b\\right) = 0\n",
    "    \\end{cases}\n",
    "\\end{align}\n",
    "\n",
    "Les solutions de ce système sont :\n",
    "\n",
    "\\begin{align}\n",
    "    a & = \\frac{\\displaystyle N \\sum_{k=1}^N x_k y_k - \\sum_{k=1}^N x_k\\sum_{k=1}^N y_k}{\\displaystyle N\\sum_{k=1}^N x_k^2 - \\left(\\sum_{k=1}^N x_k\\right)^2} &\n",
    "    b & = \\frac{\\displaystyle \\sum_{k=1}^N x_k^2 \\sum_{k=1}^N y_k - \\sum_{k=1}^N x_k\\sum_{k=1}^N x_k y_k}{\\displaystyle N\\sum_{k=1}^N x_k^2 - \\left(\\sum_{k=1}^N x_k\\right)^2}\n",
    "\\end{align}\n",
    "\n",
    "### Progression\n",
    "\n",
    "Le programme sera écrit de plusieurs façon différentes.\n",
    "\n",
    "1. Tous les calculs seront réalisés à la main.\n",
    "2. Création d'une fonction qui réalise la régression linéaire\n",
    "3. Utilisation du module numpy pour simplifier les calculs\n",
    "4. Utilisation des méthodes des modules numpy/scipy pour réaliser la régression linéaire\n",
    "5. (bonus) Utilisation du module matplotlib pour représenter les points et la droite de régression."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Programmation\n",
    "\n",
    "### Étape 0: Lecture du fichier `donnees.dat`\n",
    "\n",
    "Contenu du fichier `donnees.dat`\n",
    "\n",
    "     1.  2.1\n",
    "     2.  2.9\n",
    "     3.  4.2\n",
    "     4.  5.05\n",
    "     5.  5.85\n",
    "     6.  6.95\n",
    "     7.  8.1\n",
    "     8.  9.0\n",
    "     9. 10.2\n",
    "    10. 10.9\n",
    "    \n",
    "Lecture du fichier ligne par ligne :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"data/donnees.dat\", \"r\") as inp:\n",
    "    for line in inp:\n",
    "        xi, yi = line.split()\n",
    "        print(xi, yi)\n",
    "print(\"type de xi : \", type(xi))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On va maintenant enregistrer les valeurs xi et yi dans des listes. \n",
    "\n",
    "* Créer une liste `x` et une liste `y`\n",
    "* Remplir progressivement les listes avec la méthode `append()`\n",
    "* Il faut penser à convertir les nombres en flottants avec la fonction `float()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Afficher les listes `x` et `y` pour vérifier ce qui a été lu."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nous verrons par la suite comment lire ce type de fichier de façon efficace avec la méthode `loadtxt()` de `numpy`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Étape 1: À la main\n",
    "\n",
    "Dans cette étape on va utiliser directement les formules présentées en introduction pour calculer la valeur des paramètres $a$ et $b$. Commençons par calculer la somme des valeurs de $x$. \n",
    "\n",
    "\\begin{equation}\n",
    "    x_{sum} = \\sum_{k=1}^N x_k\n",
    "\\end{equation}\n",
    "\n",
    "Le point important est de ne pas oublier d'initialiser la valeur de la somme à zéro."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On fait de même pour la somme des valeurs de $y$ et des valeurs de $x$ au carré."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Il reste à calculer la somme des produits $x\\times y$. Pour parcourir à la fois la liste des valeurs de $x$ et de $y$ on utilise la fonction `zip` qui joint les deux listes :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "help(zip)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mettons cela à profit pour calculer la somme :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Maintenant que nous disposons de toutes les valeurs nécessaires, il ne reste plus qu'à calculer $a$ et $b$. Nous avons encore besoin du nombre de points. La fonction `len` donne le nombre d'éléments d'une liste."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En utilisant les équations présentées en introduction, calculons maintenant `a` et `b`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print(\"a = \", a)\n",
    "print(\"b = \", b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Étape 2: Créer une fonction\n",
    "\n",
    "Dans cette fonction nous allons regrouper les différentes étapes permettant de réaliser la régression linéaire. La fonction prend comme arguments les listes des valeurs de $x$ et $y$ et retourne les valeurs des paramètres $a$ et $b$. Les entrées et sorties de la fonction doivent être explicités dans la `docstring` située en dessous de la définition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def regLin(x, y):\n",
    "    \"\"\" \n",
    "    Ajuste une droite d'équation a*x + b sur les points (x, y) par la méthode\n",
    "    des moindres carrés.\n",
    "    \n",
    "    Args :\n",
    "        * x (list): valeurs de x\n",
    "        * y (list): valeurs de y\n",
    "        \n",
    "    Return:\n",
    "        * a (float): pente de la droite\n",
    "        * b (float): ordonnée à l'origine\n",
    "    \"\"\"\n",
    "    # ecrire la fonction\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Utilisons maintenant cette nouvelle fonction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a, b = regLin(x, y)\n",
    "print(\"a = \", a)\n",
    "print(\"b = \", b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pour afficher les nombres flotant, il est possible d'utiliser un format. Deux syntaxes existent suivant la version de python :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# python 3.6 et superieur (f-strings)\n",
    "print(f\"a = {a:8.3f}\")\n",
    "print(f\"b = {b:8.3f}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# python, all versions\n",
    "print(\"a = %8.3f\" % a)\n",
    "print(\"b = %8.3f\" % b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Étape 3: Utilisation du module numpy\n",
    "\n",
    "Nous allons maintenant utiliser le module numpy pour simplifier le calcul des sommes. Pour commencer il faut importer le module numpy. Il est courant de le donner `np` comme raccourci."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La somme des valeurs d'un tableau peut être obtenue par la méthode `sum()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.array([1, 2, 3])\n",
    "a.sum()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La somme des carrés ou des produits peut également être obtenue aisément."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(a ** 2).sum()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pour calculer les produits entre deux arrays numpy il suffit de les multiplier :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = np.array([3, 2, 1])\n",
    "print(\"a : \", a)\n",
    "print(\"b : \", b)\n",
    "print(\"a * b :\", a * b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La somme se calcule alors de la même manière que précédemment pour le carré :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(a * b).sum()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nous pouvons maintenant simplifier la fonction `regLin` en utilisant les fonctions de `numpy` :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def regLin_np(x, y):\n",
    "    \"\"\" \n",
    "    Ajuste une droite d'équation a*x + b sur les points (x, y) par la méthode\n",
    "    des moindres carrés.\n",
    "    \n",
    "    Args :\n",
    "        * x (list): valeurs de x\n",
    "        * y (list): valeurs de y\n",
    "        \n",
    "    Return:\n",
    "        * a (float): pente de la droite\n",
    "        * b (float): ordonnée à l'origine\n",
    "    \"\"\"\n",
    "    # ecrire la fonction\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La nouvelle fonction renvoie évidemment les mêmes résultats :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a, b = regLin_np(x, y)\n",
    "print(f\"a = {a:8.3f}\\nb = {b:8.3f}\") # \\n est le caractere de fin de ligne"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Étape 4: Utilisation des méthodes prédéfinies dans numpy et scipy\n",
    "\n",
    "Numpy et Scipy sont deux modules scientifiques de python qui regroupent de nombreuses fonctions. Nous allons utiliser la méthode [loadtxt](http://docs.scipy.org/doc/numpy/reference/generated/numpy.loadtxt.html) pour lire le fichier texte et les méthodes [polyfit](http://docs.scipy.org/doc/numpy/reference/generated/numpy.polyfit.html) et [linregress](http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.linregress.html) pour réaliser la régression linéaire. Le module numpy est totalement inclu dans scipy. Compte tenu du grand nombre de modules et bibliothèques python existants, il est important de savoir lire une documentation pour utiliser les méthodes disponibles. De plus, l'utilisation d'une méthode déjà existante accélère le travail de développement tout en évitant de refaire la même chose. Pour obtenir la documentation à l'intérieur de ipyhon, ajouter un ? après le nom de la méthode.\n",
    "\n",
    "Commençons par lire le fichier `donnees.dat` avec la méthode `loadtxt` pour lire le fichier de données."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = np.loadtxt(\"data/donnees.dat\")\n",
    "print(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les valeurs de $x$ correspondent à la première colonne."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = data[:,0]\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les valeurs de $y$ correspondent à la deuxième colonne."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = data[:,1]\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le tout peut être fait en une seul ligne :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y = np.loadtxt(\"data/donnees.dat\", unpack=True)\n",
    "print(x)\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Utilisation de la méthode `polyfit`\n",
    "\n",
    "La méthode `polytfit` du module `numpy` prend comme argument les valeurs de $x$, de $y$ et le degré du polynome (1 ici puisqu'il s'agit d'une droite). Elle retourne un ensemble de paramètres."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "help(np.polyfit)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les paramètres sont bien les mêmes que ceux que nous avions déterminés à la main."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Utilisation de la méthode `linregress`\n",
    "\n",
    "La méthode `linregress` est contenue dans le sous-module `stats` du module `scipy`. Elle prend comme argument les valeurs de $x$, de $y$ et retourne en plus des paramètres $a$ et $b$, le coefficiant de corrélation.\n",
    "\n",
    "Commençons par importer la méthode `linregress`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.stats import linregress"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "help(linregress)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Utilisons maintenant la méthode `linregress`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Représentation graphique\n",
    "\n",
    "Python offre la possibilité de réaliser une réprésentation graphique en utilisant le module [matplotlib](http://matplotlib.org/). Voici un exemple d'utilisation de ce module pour représenter les points $(x, y)$ et la droite de régression précédemment déterminée.\n",
    "\n",
    "Pour une utilisation dans jupyter il faut d'abord préparer l'environnement pour intégrer matplotlib."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Chargeons le module ``matplotlib.``"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "plt.rcParams[\"figure.figsize\"] = 8, 8  # ajuste la taille des figures\n",
    "plt.rcParams[\"font.size\"] = 14  # taille de la police des figures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Préparons maintenant le graphique."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(x, y, \"o\", label=\"donnees\") # les points (x, y) representes par des points\n",
    "plt.plot( # droite de regression\n",
    "    [x[0], x[-1]],                  # valeurs de x\n",
    "    [a * x[0] + b, a * x[-1] + b],  # valeurs de y\n",
    "    label=\"regression\")             # legende\n",
    "plt.xlabel(\"x\") # nom de l'axe x\n",
    "plt.ylabel(\"y\") # nom de l'axe y\n",
    "plt.xlim(0, 11) # échelle axe x\n",
    "plt.legend() # la legende\n",
    "plt.title(\"Regression Lineaire\") # titre de graphique"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pour aller plus loin : les résidus\n",
    "\n",
    "Nous allons maintenant utiliser les méthodes vues précédemment sur un échantillon plus large. De plus, nous représenterons les résidus qui correspondent à la différence entre les points issus des données et la droite obtenue par la régression linéaire.\n",
    "\n",
    "Commençons par lire les données dans le fichier ``regLinData.dat``"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y = np.loadtxt(\"data/regLinData.dat\", unpack=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Utilisons maintenant la fonction ``linregress`` pour trouver les paramètres de la régression linéaire :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nous allons maintenant visualiser les points et la droite de régression dans un premier graphique, puis, les résidus dans un second graphique. Pour ce faire, nous utiliserons la méthode [`subplot`](http://matplotlib.org/users/pyplot_tutorial.html#working-with-multiple-figures-and-axes) de ``matplotlib`` qui place des graphiques sur une grille. Les trois arguments de ``subplot`` sont le nombre de lignes, le nombre de colonnes et le numéro du graphique."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# graphique 1: les données et la droite de régression\n",
    "plt.subplot(2, 1, 1)\n",
    "\n",
    "### construire le graphique ici\n",
    "\n",
    "# graphique 2: les résidus\n",
    "plt.subplot(2, 1, 2)\n",
    "\n",
    "### construire le graphique des résidus ici\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les résidus sont un bon indicateur de la qualité du modèle. Ici, on dit que les résidus sont 'structurés'. Ils ne sont pas aléatoirement répartis autour de zéro, ils présentent une variation parabolique. Cette structure des résidus indique qu'une fonction affine n'est pas adaptée pour représenter les données.\n",
    "\n",
    "Utiliser la méthode `polyfit` pour ajuster une parabole, c'est à dire un polynôme de degré 2 :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reprennons les lignes précédentes pour représenter les données graphiquement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# graphique 1: les données et la droite de régression\n",
    "plt.subplot(2, 1, 1)\n",
    "\n",
    "### construire le graphique ici\n",
    "\n",
    "# graphique 2: les résidus\n",
    "plt.subplot(2, 1, 2)\n",
    "\n",
    "### construire le graphique des résidus ici\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Cette fois, les résidus sont bien répartis aléatoirement autour de l'origine. Calculons le coefficient de détermination selon\n",
    "\n",
    "\\begin{equation}\n",
    "    R^2 = \\frac{\\displaystyle\\sum_k (y^{calc}_k - \\overline{y})^2}{\\displaystyle\\sum_k (y_k - \\overline{y})^2}\n",
    "\\end{equation}\n",
    "\n",
    "où les $y_k$ sont les données, les $y^{calc}$ sont ceux calculés avec le polynômes et $\\overline{y}$ est la moyenne des valeurs $y_k$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comme attendu le coefficient de détermination est meilleur que celui obtenu pour la droite (0.9916). Pour information, la fonction utilisée pour produire les données du fichier ``regLinData.dat`` est :\n",
    "\n",
    "\\begin{equation}\n",
    "    f(x) = 0.08 x^2 + 2x + 3 + \\hat{g}(X)\n",
    "\\end{equation}\n",
    "\n",
    "où $\\hat{g}(X)$ est un bruit gaussien obtenu par le tirage de nombres aléatoires dans une distribution gaussienne centrée en zéro et de largeur 2. Comparer les paramètres obtenus avec `polyfit` avec ceux de la fonction."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "Ce TP est terminé. D'un point de vue théorique il permet de découvrir ou réviser la regression linéaire. D'un point de vue pratique, il vous a permis de mettre en pratique la syntaxe du langage python et d'utiliser des modules tels que ``numpy``, ``scipy`` ou ``matplotlib``."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
